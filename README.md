# IFN1_sensitvity analysis

* The sensitivity analysis of IFN1 boolean model is performed using RMut packages against the overexpression and knockout mutations. Then the simulation is performed based on the ranked sensitivity values on CellCollective platform. Attractor and structural analysis are also performed using synchronous updating scheme to support the main conclusions. 

* OpenCL library is used to utilize the full computing power of multi-core CPUs and GPUs to fasten the computation in parallel with your device.